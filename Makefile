SHELL := /usr/bin/env bash

thisdir := $(abspath $(dir $(realpath $(firstword $(MAKEFILE_LIST)))))

${thisdir}/%.bash.txt: ${thisdir}/%.bash
	echo                      >  $@
	echo "### created-by: $<" >> $@
	$<                        >> $@

$(shell rm -f ${thisdir}/*.bash.txt)

bash-files :=     $(wildcard ${thisdir}/*.bash)
bash-txt-files := $(addsuffix .txt,${bash-files})
txt-files :=      $(wildcard ${thisdir}/*.txt)
all-txt-files :=  $(sort ${bash-txt-files} ${txt-files})

.PHONY: all clean install

all: ${thisdir}/hosts

${thisdir}/hosts: ${all-txt-files}
	rm -f ${thisdir}/hosts
	for file in ${all-txt-files}; do cat $$file >> ${thisdir}/hosts; done

clean:
	rm -f ${bash-txt-files} ${thisdir}/hosts

install:  ${thisdir}/hosts
	sudo cp ${thisdir}/hosts /etc/
	sudo chown root:root     /etc/hosts # on macos this needs to be root:wheel
	sudo chmod 0644          /etc/hosts
