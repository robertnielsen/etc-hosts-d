#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-
set -o errexit -o nounset -o pipefail
shopt -s inherit_errexit 2> /dev/null || { echo "$0: error: bash 4.4 or later required" 1>&2; exit 1; }

thisdir=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)

function start() {
  declare script=~/vaark/examples/kafka/bin/cluster-hosts.bash
  if ! test -e $script; then
    echo "${BASH_SOURCE[0]}: error: Cannot find vaark script '$script'" 1>&2
    exit 1
  fi
  $script
}
if test "$0" == "${BASH_SOURCE[0]:-$0}"; then
  start "$@"
fi
